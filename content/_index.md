---
date: 2020-05-17T15:45:00Z
lastmod: 2020-05-18T15:45:00Z
publishdate: 2020-05-17T15:45:00Z

title: Main
description: NeoBabel Wiki Landing Page
---

# NeoBabel

## A System Agnostic Hard SciFi World

{{% panel %}}
This wiki is under **heavy** modification, and as such all links may not work.
It will take some time for things to be fleshed out in such a way that the wiki
can be operated by clicking links within articles. In the meantime, please use
the links to the left to navigate.
{{% /panel %}}

## Design Goals

* Most concepts introduced should be citable to real world concepts
* No more than 10% of the setting should be "hand waved" away
* Users should be able to make sense of the "current" world from historical events
* Campaigns should be able to be dropped into any part of the world and have sufficient information to run successfully
* Governments should make sense based on current political knowledge/comparative politics
* Market forces should be explainable (Competition, Economic Systems)
* Released under [Creative Commons Zero][1] (Public Domain)
* World should be capable of being used in any system from Shadowrun to D20 Modern

[1]: https://creativecommons.org/publicdomain/zero/1.0/
