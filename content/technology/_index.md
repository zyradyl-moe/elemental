---
title: "Technology"
date: 2020-05-17T15:45:00Z
lastmod: 2020-05-18T15:45:00Z
publishdate: 2020-05-17T15:45:00Z
draft: false
weight: 10
---

Technology plays a vital role in public society in the world of [NeoBabel][4].
In fact, since the passage of the [One World, One People (OWOP)][1] Act of
<span style="color:red">????</span>, any child born through proper means has
received two modifications on birth, the [Babel Implant][2] and the
[Subcutaneous Chemical Administration Port][3]. These two forms of technology
are just a taste of the innovation and advancement present within NeoBabel.

## [Artificial Intelligence (AI)][5]
Far from science fiction, Artificial Intelligence has become a common, albeit
publicly _loathed_ part of the modern world. While the early 21st century
resulted in some forms of primitive "[Virtual Intelligences][6]" it wasn't until
the [Singularity][7] in <span style="color:red">????</span> that Artificial
Intelligence in its current form came into existence.

## [Babel Implant][2]
Essentially a high-tech cochlear implant, the Babel Implant was designed to
eliminate the barrier of human language. An invention of the
[Human Aptitude and Longevity Optimization Program][8], this implant translates
all the different languages an individual may hear into their native tongue. In
this way, the Babel Implant was believed to strike a balance between protecting
individuals' cultural identity, while still allowing effective communication
within the megacities that resulted from the
[Regional Unification and Modernization Initiative][9].

## [Climate Analysis, Control, Engineering, and Protection System (CACEPS)][10]
A product of the [Global Engineering and Protection Corporation][11], CACEPS
is the climate engineering and weather modification device that helps to keep
the megacities safe. Incredibly complex, and incredibly massive, CACEPS are no
longer actively constructed due to the immense amount of funding that would be
required. A system composed of automated drone aircraft, multiple geosynchronous
satellites, banks upon banks of computers, and incredibly complex equations,
CACEPS touches every single life within the megacities, and if it is functioning
as intended, should be unnoticeable.

## [Laminar Citites][12]
A secondary product of [GEP Co.][11], Laminar Cities are the environment in
which the vast majority of the global population lives. Composed of identical
rounded buildings arranged in a grid of forty two square kilometers, each
alphanumeric district is composed of four buildings 490m square and 2km tall,
with a 20m road of four lanes sectioning each building off. The city is
outlined by a similar 20m road, which then connects to the interregional
transit systems. The cities are so named because weather systems that collide
with the cities tend to be channeled down one set of the streets, similar to
a laminar air flow.

## [Subcutaneous Chemical Administration Port][3]
To Be Developed.

## [Virtual Intelligence][6]
To Be Developed.

[1]: /laws/owop/
[2]: /technology/babel/
[3]: /technology/scap/
[4]: /about/
[5]: /technology/ai/
[6]: /technology/vi/
[7]: /history/singularity/
[8]: /corporations/haloc/
[9]: /laws/rumi/
[10]: /technology/caceps/
[11]: /corporations/gep/
[12]: /technology/laminar/
